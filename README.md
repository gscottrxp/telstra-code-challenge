# Telstra Code Challenge

Shopping cart code challenge

## Getting Started

Clone repo 

```

git clone https://gscottrxp@bitbucket.org/gscottrxp/telstra-code-challenge.git

```


* npm install
* npm run start

### Prerequisites

VS Code or any IDE


## Tests

* npm run test
