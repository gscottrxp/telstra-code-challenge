import React from 'react';
import Product from './Product';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions';

import productJSONData from '../../data/product-data.json';

class MainPanel extends React.Component {

    /**
     * @description compare the product array in the store to the productName
     *  and filter out any productName that matches product names in the store
     * @param {array} productData
     * @returns {boolean}
     */
    filterCartProducts(productData) {
        const arr = this.props.cartProductNames.filter(product => product === productData.productName);
        return arr.length < 1;
    }

    /**
     * @description render product data from the json file
     * will filter out any products already in the shopping cart
     */
    renderProduct() {
        return productJSONData
            .filter(product => product.isPublished === 'true')
            .filter(productData => this.filterCartProducts(productData))
            .map(productData => (
                <Product key={productData.productName}
                    productName={productData.productName}
                    productImageUrl={productData.productImage}
                    productPrice={productData.price}
                    addToCart={this.props.addToCart}
                />
            ))
    }

    render() {
        return (
            <div className="main-panel row col-12 col-md-8">
                {this.renderProduct()}
            </div>
        );
    }
}

/**
 * @description connect to redux store and retrieve the current product names
 * in the cart
 * @param {Object} state
 */
const mapStateToProps = state => {
    return {
        cartProductNames: state.products
    };
};

/**
 * @description addToCart function to be passed down to each product
 * @param {Action} dispatch
 */
const mapDispatchToProps = dispatch => {
    return {
        addToCart: props => dispatch(actionCreators.addToCart(props)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MainPanel);
