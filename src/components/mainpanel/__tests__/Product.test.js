import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Root from '../../../Root';

import Product from '../Product';
let wrapped;

configure({
  adapter: new Adapter()
});

beforeEach(() => {

  const productName = 'phone 1';
  const url = "https://www.telstra.com"
  const price = '199';

  wrapped = mount(
    <Root>
      <Product
        productName={productName}
        productImageUrl={url}
        productPrice={price}
      />
    </Root>
  );
});

afterEach(() => {
  wrapped.unmount();
})

describe('Product Component', () => {

  it('find div in Product component', () => {
    expect(wrapped.find('div')).toBeTruthy();
  });

  it('product header should contain product name and price', () => {
    expect(wrapped.find('.product-header').first().text()).toContain('phone 1');
    expect(wrapped.find('.product-price').first().text()).toContain('199');
  });
});
