import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Root from '../../../Root';

import MainPanel from '../MainPanel';
let wrapped;

configure({
    adapter: new Adapter()
});

beforeEach(() => {
    wrapped = mount(
        <Root>
            <MainPanel
            />
        </Root>
    );
});

afterEach(() => {
    wrapped.unmount();
})

describe('MainPanel Component', () => {

    it('find div in MainPanel component', () => {
        expect(wrapped.find('div')).toBeTruthy();
    });

});
