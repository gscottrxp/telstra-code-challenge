import React from 'react';

/**
 * @description component for individual product tiles
 * @param {Object} props
 */
const Product = props => {

  const { productName, productImageUrl, productPrice } = props;
  return (
    <div className="product-container col-6 col-md-5 col-lg-3">
      <div className="product-border">
        <div className="product-header">{productName}</div>
        <img className="product-image" src={productImageUrl} alt={productName} title={productName} />
        <div className="product-price">${productPrice}</div>
        <button type="button" className="btn btn-primary" onClick={() => props.addToCart(productName)} title="Add to cart ">Add to cart</button>
      </div>
    </div>
  );
}

export default (Product);