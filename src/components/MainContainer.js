import React from 'react';
import Cart from './cart/Cart';
import MainPanel from './mainpanel/MainPanel'

/**
 * @description, the main container that holds the main panel and cart containers
 */
const MainContainer = () => {
    return (
        <div className="main-container row">
            <MainPanel />
            <Cart />
        </div>
    );
}

export default (MainContainer);
