import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Root from '../../../Root';

import Cart from '../Cart';
let wrapped;

configure({
    adapter: new Adapter()
});

beforeEach(() => {

    const initialState = { products: ['product 1'] };

    wrapped = mount(
        <Root initialStat={initialState}>
            <Cart
            />
        </Root>
    );
});

afterEach(() => {
    wrapped.unmount();
})

describe('Cart Component', () => {

    it('find div in Cart component', () => {
        expect(wrapped.find('div')).toBeTruthy();
    });

    it('expect to find cart-product-heading', () => {
        expect(wrapped.find('.cart-product-heading')).toBeTruthy();
    });
});
