import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Root from '../../../Root';

import CartItem from '../CartItem';
let wrapped;

configure({
  adapter: new Adapter()
});

beforeEach(() => {

  const productName = 'phone 1';

  wrapped = mount(
    <Root>
      <CartItem
        productName={productName}
      />
    </Root>
  );
});

afterEach(() => {
  wrapped.unmount();
})

describe('CartProduct Component', () => {

  it('find div in CartProduct component', () => {
    expect(wrapped.find('div')).toBeTruthy();
  });

  it('product header should contain product name phone 1', () => {
    expect(wrapped.find('.cart-product-heading').first().text()).toContain('phone 1');
  });


});
