import React from 'react';

/**
 * @description single product item in cart
 * clicking 'remove' will activate the removeFromCart function in the parent component
 * @param {Object} props
 */
const CartItem = props => {
    return (
        <div className="cart-product-container" >
            <span className="cart-product-heading">{props.productName}</span>
            <button type="button"
                className="btn btn-secondary btn-sm cart-btn" onClick={() => props.removeFromCart(props.productName)}
                title="Remove from cart" >Remove</button>
        </div>
    )
}

export default CartItem;
