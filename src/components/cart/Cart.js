import React from 'react';
import CartItem from './CartItem';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions';

/**
 * @description displays the products currently in the shopping cart
 * the product data is stored in the redux store
 */
class Cart extends React.Component {

    /**
     * @description map through the products currently in the store and
     * use the CartProduct component to display items
     */
    renderCartProducts() {
        return this.props.cartProductNames
            .map(productName => (
                <CartItem key={productName}
                    productName={productName}
                    removeFromCart={this.props.removeFromCart}
                />
            ))
    }

    render() {
        return (
            <div className="cart-container col-md-4">
                <div className="cart-border">
                    <span className="cart-heading">Shopping cart</span>
                    {this.renderCartProducts()}
                </div>
            </div>
        );
    }
}

/**
 * @description connect to redux store and retrieve the current product names
 * in the cart
* @param {Object} state
*/
const mapStateToProps = state => {
    return {
        cartProductNames: state.products
    };
};

/**
 * @description removeFromCart function to be passed down to the individual cart items
* @param {Action} dispatch
*/
const mapDispatchToProps = dispatch => {
    return {
        removeFromCart: props => dispatch(actionCreators.removeFromCart(props)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Cart);
