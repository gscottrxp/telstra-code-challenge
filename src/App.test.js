import React from 'react';
import App from './App';
import Adapter from 'enzyme-adapter-react-16';
import Root from './Root';
import { shallow, configure } from 'enzyme';

configure({
  adapter: new Adapter()
});

describe('AUTH ', () => {
  let wrapped;

  wrapped = shallow(
    <Root>
      <App />
    </Root>
  );

  it('renders without crashing', () => {
    wrapped.find('div');
  });
});