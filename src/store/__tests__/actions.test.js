import * as actions from "../actions";
import * as actionTypes from "../actionTypes";


describe("Actions test", () => {

  it("addToCart() returns type ADD_TO_CART,,", () => {
    const authData = { product: 'iphone mobile' };
    const action = actions.addToCart(authData); 

    expect(action.type).toEqual(actionTypes.ADD_TO_CART);
  });

  it("addToCart() returns type REMOVE_FROM_CART,,", () => {
    const authData = { product: 'iphone mobile' };
    const action = actions.removeFromCart(authData); 

    expect(action.type).toEqual(actionTypes.REMOVE_FROM_CART);
  });
  
});
