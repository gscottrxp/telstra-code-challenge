import reducer from '../reducer';
import * as actionTypes from '../actionTypes';

describe('reducer test ', () => {

  it('returns initial default state', () => {
    const newState = reducer(undefined, {});
    expect(newState).toBeTruthy();
  });

  it('addToCart() reducer returns a newState', () => {
    const state = {
      products: ['phone 1', 'phone 2']
    };
    const newState = reducer(state, actionTypes.ADD_TO_CART);
    expect(newState).toBeTruthy();
  });

  it('addToCart reducer returns array with phone 3 added to products array', () => {
    const initialState = {
      products: ['phone 1', 'phone 2']
    };

    const action = {
      type: 'ADD_TO_CART',
      product: 'phone 3'

    }

    const newArray = {products: ['phone 1', 'phone 2', 'phone 3']};
    const newState = reducer(initialState,action);
    expect(newState).toEqual(newArray);
  });

  it('removeFromCart reducer returns array with phone 2 deleted from the products array', () => {
    const initialState = {
      products: ['phone 1', 'phone 2']
    };

    const action = {
      type: 'REMOVE_FROM_CART',
      product: 'phone 2'

    }

    const newArray = {products: ['phone 1']};
    const newState = reducer(initialState,action);
    expect(newState).toEqual(newArray);
  });

});
