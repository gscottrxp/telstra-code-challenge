import * as actionTypes from './actionTypes';

/**
 * @description action to add a product to the cart
 * @param {String} product
 */
export const addToCart = (product) => {
    return {
        type: actionTypes.ADD_TO_CART,
        product
    };
}

/**
 * @description action remove a product from the cart
 * @param {String} product
 */
export const removeFromCart = (product) => {
    return {
        type: actionTypes.REMOVE_FROM_CART,
        product
    };
}