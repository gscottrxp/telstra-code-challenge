import * as actionTypes from './actionTypes';

const initialState = {
    products: []
}

/**
 * @description add a product to products array in the store
 * @param {Object} state
 * @param {Action} action
 */
export const addToCart = (state, action) => {
    return {
        ...state,
        products: [
            ...state.products,
            action.product
        ]
    };
};

/**
 * @desctiption removes product from the store
 * @param {Object} state
 * @param {Action} action
 */
export const removeFromCart = (state, action) => {
    return {
        ...state,
        products: [
            ...state.products.filter(product => product !== action.product)
        ]
    };
};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case actionTypes.ADD_TO_CART:
            return addToCart(state, action);
        case actionTypes.REMOVE_FROM_CART:
            return removeFromCart(state, action);
        default:
            return state;
    }
};

export default reducer;

